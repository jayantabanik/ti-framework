<?php
/**
 * @file
 * Functions that need to be loaded on every themeinnova framework request.
 */
//all define
define('TI_VERSION', '1.0.0');
define('TI_REALPATH', realpath(__FILE__));
define('TI_BASEPATH', TEMPLATEPATH."/ti_frame/");
define('TI_ENVIRONMENT', 'test');
define('TI_ERROR', TI_BASEPATH.'errors/');
define('TI_EXTENSION', 'tpl');
define('MODPATH', TI_BASEPATH.'modules/');
if(!defined("IS_ADMIN"))
    define("IS_ADMIN",  is_admin());

//load common
require_once(TI_BASEPATH.'core/common.php');
$TIN = & load_class('Themeinnova');

//load constants
if (file_exists(TI_BASEPATH.'config/'.TI_ENVIRONMENT.'/constants.php')){
	require_once(TI_BASEPATH.'config/'.TI_ENVIRONMENT.'/constants.php');
}
else{
	require_once(TI_BASEPATH.'/config/constants.php');
}

//load controller
require_once TI_BASEPATH.'core/Controller.php';
function &get_instance(){
	return TI_Controller::get_instance();
}
//load all libs
require_once TI_BASEPATH.'core/libs.php';
