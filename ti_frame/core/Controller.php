<?php if ( ! defined('TI_BASEPATH')) exit('No direct script access allowed');

class TI_Controller {
	private static $instance;

	public function __construct(){
		self::$instance =& $this;
		
		$this->load =& load_class('Loader');

		$this->load->initialize();
	}

	public static function &get_instance(){
		return self::$instance;
	}
}
