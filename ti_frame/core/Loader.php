<?php  if ( ! defined('TI_BASEPATH')) exit('No direct script access allowed');

class TI_Loader {

	protected $_ti_ob_level;
	protected $_ti_view_paths		= array();
	protected $_ti_model_paths		= array();
	protected $_base_classes		= array();
	protected $_ti_classes			= array();
	protected $_ti_models			= array();

	public function __construct(){
		$this->_ti_ob_level  = ob_get_level();
		$this->_ti_model_paths = array(MODPATH);
		$this->_ti_view_paths = array(MODPATH	=> TRUE);
	}
	public function initialize(){
		$this->_ti_classes = array();
		$this->_ti_models = array();
		$this->_base_classes =& is_loaded();

		$this->_ti_autoloader();

		return $this;
	}
	public function is_loaded($class){
		if (isset($this->_ti_classes[$class])){
			return $this->_ti_classes[$class];
		}
		return FALSE;
	}
	public function model($model, $name = ''){ 
		if (is_array($model)){
			foreach ($model as $babe){
				$this->model($babe);
			}
			return;
		}

		if ($model == ''){
			return;
		}

		$path = '';
		
		if ($name == ''){
			$name = $model;
		}

		if (in_array($name, $this->_ti_models, TRUE)){
			return;
		}

		$ti =& get_instance();
		if (isset($ti->$name)){
			show_error('The model name you are loading is the name of a resource that is already being used: '.$name);
		}

		$model = strtolower($model);

		foreach ($this->_ti_model_paths as $mod_path)
		{
			//echo $mod_path.$model.'/'.$path.$model.'.model';
			if ( ! file_exists($mod_path.$model.'/'.$path.$model.'.model')){
				continue;
			}

			if ( ! class_exists('CI_Model'))
			{
				load_class('Model', 'core');
			}

			require_once($mod_path.$model.'/'.$path.$model.'.model');

			$model = ucfirst("TI_".$model."_model");

			$ti->$name = new $model();

			$this->_ti_models[] = $name;
			
			return;
		}
	}
	

	
	private function _ti_autoloader(){
		if (defined('TI_ENVIRONMENT') AND file_exists(MODPATH.'config/'.TI_ENVIRONMENT.'/autoload.php')){
			include(MODPATH.'config/'.TI_ENVIRONMENT.'/autoload.php');
		}
		else{
			include(MODPATH.'config/autoload.php');
		}

		if ( ! isset($autoload)){
			return FALSE;
		}


		// Load any custom config file
		/*if (count($autoload['config']) > 0){
			$TI =& get_instance();
			foreach ($autoload['config'] as $key => $val)
			{
				$TI->config->load($val);
			}
		}*/

		// Autoload models
		if (isset($autoload['model'])){
			$this->model($autoload['model']);
		}
	}

	protected function _ti_object_to_array($object){
		return (is_object($object)) ? get_object_vars($object) : $object;
	}

	protected function &_ti_get_component($component){
		$TI =& get_instance();
		return $TI->$component;
	}
	
	public function view($view, $vars = array(), $return = FALSE){
		return $this->_ti_load(array('_ti_view' => $view, '_ti_vars' => $this->_ti_object_to_array($vars), '_ti_return' => $return));
	}
	
	protected function _ti_load($_ti_data){
		
		
		foreach (array('_ti_view', '_ti_vars', '_ti_path', '_ti_return') as $_ti_val){
			$$_ti_val = ( ! isset($_ti_data[$_ti_val])) ? FALSE : $_ti_data[$_ti_val];
		}

		$file_exists = FALSE;

		$_ti_ext = pathinfo($_ti_view, PATHINFO_EXTENSION);
		$_ti_file = ($_ti_ext == '') ? $_ti_view.'.'.TI_EXTENSION : $_ti_view;

		foreach ($this->_ti_view_paths as $view_file => $cascade){
		
		
			if (file_exists($view_file.$_ti_file)){
				$_ti_path = $view_file.$_ti_file;
				$file_exists = TRUE;
				break;
			}

			if ( ! $cascade){
				break;
			}
		}
		

		if ( ! $file_exists && ! file_exists($_ti_path)){
			show_error('Unable to load the requested file: '.$_ti_file);
		}
		
		extract($_ti_vars);
		ob_start();
		include($_ti_path);
		$buffer = ob_get_contents();
		@ob_end_clean();
		
		echo $buffer;
	}

}

/* End of file Loader.php */
/* Location: ./ti_frame/core/Loader.php */
