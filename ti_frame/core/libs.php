<?php
global $ti; 
$all_active_modules = array();
$array = activatedModulesList();
$all_active_modules = array_merge($all_active_modules, $array);

foreach($all_active_modules as $all_active_module){
	if( file_exists($ti->libs.$all_active_module."/".$all_active_module.".lib")){
		include $ti->libs.$all_active_module."/".$all_active_module.".lib";
		$ti->$all_active_module = & load_class($all_active_module);
	}
}
