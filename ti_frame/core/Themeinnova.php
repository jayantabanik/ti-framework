<?php if ( ! defined('TI_BASEPATH')) exit('No direct script access allowed');

class TI_Themeinnova {
	private $constants = array();
	
	public function __construct() {
		global $ti;
		$ti = new stdClass;
		add_action( 'admin_init', array( &$this, 'admin_init' ) );
		add_action('admin_menu', array(&$this,'admin_menu'));
	}
	
	public function admin_init(){
		 $this->_installTheme();
    }
    
    public function _installTheme(){
    	ti_theme_setup();
    }
	public function __set($name, $value){
        $this->constants[$name] = $value;
    }
    
    public function __get($name){
        if (array_key_exists($name, $this->data)) {
            return $this->constants[$name];
        }
		else{
			return null;
		}
    }

   
    public function __isset($name){
        return isset($this->constants[$name]);
    }
    
    public function __unset($name){
        unset($this->constants[$name]);
    }
    
    public function admin_menu(){	
    		
		$all_active_modules = array();
		$array = activatedModulesList();
		$all_active_modules = array_merge($all_active_modules, $array);	
		
		add_object_page(__('Ti', "themeinnova"), __('Ti - Basic', "themeinnova"), 'administrator', 'ti-basic' , array($this, 'main'), '');	
		foreach($all_active_modules as $class){ 
			$$class = & load_class($class);
			$func = $class."_menu";
			$menus = $$class->$func();
			
			if(is_array($menus)){
				foreach($menus as $menu){
					$call_back = isset($menu['call_back'])?$menu['call_back']:'main';
					add_submenu_page('ti-basic', $menu['page_title'], $menu['menu_title'], $menu["capability"], $menu['menu_slug'], array($$class, $call_back));
				}
			}
		}
    }
    
    public function getModuleList(){
		global $wpdb, $ti;
		$modules = array();
		$results = $wpdb->get_results("SELECT * FROM {$ti->prefix}_system where type='module' and status= 1");

		foreach ($results as $result) {
			$modules[] = $result->filename;
		}
		return $modules;
	}
	
	private function globalizeAllClasses(){
		$all_active_modules = array();
		$array = activatedModulesList();
		$all_active_modules = array_merge($all_active_modules, $array);	
		foreach($all_active_modules as $class){ 
			$$class = & load_class($class);
		}
	}
	
	public function main(){
		echo "test";
	}
}
