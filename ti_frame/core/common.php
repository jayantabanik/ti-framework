<?php

if ( ! function_exists('load_class')){
	function &load_class($class, $directory = 'core', $prefix = 'TI_'){
		static $_classes = array();

		
		if (isset($_classes[$class])){
			return $_classes[$class];
		}

		$name = FALSE;
		foreach (array(TI_BASEPATH, MODPATH) as $path){
	
			if (file_exists($path.$directory.'/'.$class.'.php')){
				$name = $prefix.$class;

				if (class_exists($name) === FALSE){
					require($path.$directory.'/'.$class.'.php');
				}

				break;
			}
			
			if (file_exists($path.$class."/".$class.'.con')){
				$name = $prefix.$class;

				if (class_exists($name) === FALSE){
					require($path.$class."/".$class.'.con');
				}

				break;
			}
		}
		
		if ($name === FALSE){
		$caller = current(debug_backtrace());
			printf ( 'Debugging from file :%s line %d',  $caller['file'], $caller['line']);
			exit('Unable to locate the specified class: '.$class.'.php');
		}
		is_loaded($class);
		$_classes[$class] = new $name();
		return $_classes[$class];
	}
}

if ( ! function_exists('is_loaded')){
	function is_loaded($class = '')
	{ static $_is_loaded = array();

		if ($class != '')
		{
			$_is_loaded[strtolower($class)] = $class;
		}

		return $_is_loaded;
	}
}

if ( ! function_exists('ti_theme_setup')){
	function ti_theme_setup($force_setup = false){
       global $wpdb,$ti;

        $version = $ti->version;

        if(get_option("{$ti->prefix}_theme") != $version || $force_setup){

            $error = "";
            if(!ti_database_permission($error)){
                ?>
                <div class='error' style="padding:15px;"><?php echo $error?></div>
                <?php
            }

            require_once(ABSPATH . '/wp-admin/includes/upgrade.php');

            if ( ! empty($wpdb->charset) )
                $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
            if ( ! empty($wpdb->collate) )
                $charset_collate .= " COLLATE $wpdb->collate";
            //insert query
            
            $sql = "CREATE TABLE {$ti->prefix}_system (
                  filename varchar(255) NOT NULL default '',
				  name varchar(255) NOT NULL default '',
				  type varchar(255) NOT NULL default '',
				  description varchar(255) NOT NULL default '',
				  status int(2) NOT NULL default '0',
                  weight int(2) NOT NULL default '0',
				  PRIMARY KEY (filename),
				  KEY (weight)
                ) $charset_collate;";
            
            dbDelta($sql);
             $sql = "Insert into {$ti->prefix}_system 
		        		values('load_modules',
		        				'load_modules',
		        				'module',
		        				'Module to load all other modules',
		        				'1',
		        				'1')";
			//end insert query
			$wpdb->query($sql);
			//end insert query
            update_option("{$ti->prefix}_theme", $version);
        }
	}
}

if ( ! function_exists('ti_module_setup')){
	function ti_module_setup($_ti_module, $force_setup = false){
       global $wpdb,$ti;

        $version = $ti->version;
		if($_ti_module != ''){
		    if(get_option("{$ti->prefix}_{$_ti_module}") != $version || $force_setup){

		        $error = "";
		        if(!ti_database_permission($error)){
		            ?>
		            <div class='error' style="padding:15px;"><?php echo $error?></div>
		            <?php
		        }

		        require_once(ABSPATH . '/wp-admin/includes/upgrade.php');

		        if ( ! empty($wpdb->charset) )
		            $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
		        if ( ! empty($wpdb->collate) )
		            $charset_collate .= " COLLATE $wpdb->collate";
		        //insert query
		        
		        $sql = "CREATE TABLE {$ti->prefix}_system (
		              filename varchar(255) NOT NULL default '',
					  name varchar(255) NOT NULL default '',
					  type varchar(255) NOT NULL default '',
					  description varchar(255) NOT NULL default '',
					  status int(2) NOT NULL default '0',
		              weight int(2) NOT NULL default '0',
					  PRIMARY KEY (filename),
					  KEY (weight)
		            ) $charset_collate;";
		        
		        //dbDelta($sql);
		        $sql = "INSERT into {$ti->prefix}_system 
		        		values('load_modues',
		        				'load_modues',
		        				'module',
		        				'Module to load all other modules',
		        				'1',
		        				'1')";
				//end insert query
				echo $sql;
				dbDelta($sql);
		        update_option("{$ti->prefix}_theme", $version);
		    }
        }
	}
}

if ( ! function_exists('ti_database_permission')){
    function ti_database_permission(&$error){
        global $wpdb;

        $wpdb->hide_errors();

        $ti_perm = true;

        $sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}ti_test ( col1 int )";
        $wpdb->query($sql);
        $error = "No permissions to create tables.";
        if(!empty($wpdb->last_error))
            $ti_perm = false;

        $sql = "ALTER TABLE {$wpdb->prefix}ti_test ADD COLUMN " . uniqid() ." int";
        $wpdb->query($sql);
        $error = "No permissions to modify (ALTER) tables.";
        if(!empty($wpdb->last_error))
            $ti_perm = false;

        $sql = "DROP TABLE {$wpdb->prefix}ti_test";
        $wpdb->query($sql);

        $wpdb->show_errors();

        return $ti_perm;
    }
}
/*
@returns all module names reading the module directory
*/
if ( ! function_exists('allModulesList')){
	function allModulesList($dir){
	global $ti;
	$core_modules =$ti->core;
	   $modules = scandir($dir);
	   $array = array();
		foreach($modules as $module){ 
			if($module != '.' && $module != '..' && !(in_array($module, $core_modules))){ 
				$array[] = array('filename' => $module, 'name' => ucwords(str_replace('_', ' ', $module)));
			}  
		}
		
		return $array;
	} 
}

if(!function_exists('activatedModulesList')){
	function activatedModulesList(){
		global $wpdb, $ti;
		$modules = array();
		$results = $wpdb->get_results("SELECT * FROM {$ti->prefix}_system where type='module' and status= 1 ORDER BY `weight`");

		foreach ($results as $result) {
			$modules[] = $result->filename;
		}
		return $modules;
	}
}
if ( ! function_exists('show_error')){
	function show_error($message, $heading = 'An Error Was Encountered'){
		
		$message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';

		if (ob_get_level())
		{
			ob_end_flush();
		}
		ob_start();
		include(TI_ERROR.'error_general.php');
		$buffer = ob_get_contents();
		@ob_end_clean();
		echo $buffer;
		exit;
	}
}

if ( ! function_exists('ti_redirect'))
{
	function ti_redirect($uri = '', $method = 'location', $http_response_code = 302){
		if (ob_get_level()){
			ob_end_flush();
		}
		
		header("Location: ".$uri, TRUE, $http_response_code);
		exit;
	}
}
function test(){
	global $ti;
    echo $ti->version;
}

if ( ! function_exists('ti_call')){
	//function 
}

//added on 9-11-12

add_action( 'admin_init', 'js_sortable_admin_init' );
function js_sortable_admin_init(){
	wp_enqueue_script( 'jquery-ui-sortable' );
}

function module_dir_url( $file ) {
    return trailingslashit( modules_url( '', $file ) );
}

function modules_url($path = '', $module = '') {

	$module_dir = basename(dirname($module));
	$url = WP_CONTENT_URL.'/themes/themeinnova/ti_frame/modules/';
	
	if ( 0 === strpos($url, 'http') && is_ssl() )
			$url = str_replace( 'http://', 'https://', $url );

	return $url.$module_dir;
}

function module_alter(&$item1, $key, $fix){
    $item1 = "$fix[0]$item1$fix[1]";
}