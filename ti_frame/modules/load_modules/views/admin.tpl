
<form action="admin.php?page=ti-modules.php" method="post">
<input type="hidden" name="action" value="modulestatus">

<div class = "wrap">
		<h2>All Modules</h2>
		<table class= "wp-list-table widefat fixed posts js_sort-list">
			<thead>
				<tr>
					<th>Name</th>
					<th>Order</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Name</th>
					<th>Order</th>
				</tr>
			</tfoot>
			<tbody>
			<?php foreach ($modules as $k => $mod):?>
			<tr id="list-item-<?php echo $k?>" class="list-item"><td><input <?php if(in_array($mod['filename'], $active_modules)):?>checked='checked' <?php endif;?> type="checkbox" name="modules[<?php echo $mod['filename'];?>]"></td><td> <?php echo $mod['name'];?></td></tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
</ul>


<input type="submit" value="Save Changes" class="button-primary" id="submit" name="submit">
</form>
