<?php if ( ! defined('TI_BASEPATH')) exit('No direct script access allowed');

//define all constans
global $ti;
$ti->version = '1.0.0';
$ti->prefix = 'ti';
$ti->is_admin = is_admin();
$ti->core = array('config', 'load_modules');
$ti->libs = TI_BASEPATH.'modules/';
